#!/bin/bash 
operation=$1

if [ ${operation} == "addDir"  ] 
then 
	parent_directory=$2
	child_directory=$3
	echo " user wants to create the directory "
	mkdir ${parent_directory}/${child_directory}
fi

if [ ${operation} == "listFiles"  ] 
then 
	parent_directory=$2
	
	echo " Listing all the files "
	ls -lA ${parent_directory} | grep -v '^d'
fi

if [ ${operation} == "listDirs"  ] 
then 
	parent_directory=$2
	
	echo " Listing all the directory "
	ls -lA ${parent_directory} | grep '^d'
fi

if [ ${operation} == "listAll"  ] 
then 
	parent_directory=$2
	
	echo " Listing all files and directory "
	ls -lA ${parent_directory}
fi

if [ ${operation} == "deleteDir"  ] 
then 
	parent_directory=$2
	child_directory=$3
	
	echo " Deleting a directory "
	rm -rf ${parent_directory}/${child_directory}
fi

if [ ${operation} == "addFile"  ] 
then 
	parent_directory=$2
	file=$3
	
	echo " Adding a file to a directory "
	cd ${parent_directory}
	touch ${file}
fi

if [ ${operation} == "addFile"  ] 
then 
	parent_directory=$2
	file=$3
	content=$4
	
	echo " Adding content to the file  "
	echo " ${content}" > ${parent_directory}/${file}
fi

if [ ${operation} == "addContentToFile"  ] 
then 
	parent_directory=$2
	file=$3
	content=$4
	
	echo " Adding more content  "
	echo " ${content}" >> ${parent_directory}/${file}
fi

if [ ${operation} == "addContentToFileBegining"  ] 
then 
	parent_directory=$2
	file=$3
	content=$4
	
	echo " Adding content to the beginning of a file  "
	echo " ${content}" >> ${parent_directory}/${file}.tmp
	cat ${parent_directory}/${file} >> ${parent_directory}/${file}.tmp
	cat ${parent_directory}/${file}.tmp > ${parent_directory}/${file}
fi

if [ ${operation} == "showFileBeginingContent"  ] 
then 
	parent_directory=$2
	file=$3
	NUM=$4
	
	echo " Displaying the content at the beginning of a file  "
	head -${NUM} ${parent_directory}/${file}
fi

if [ ${operation} == "showFileEndContent"  ] 
then 
	parent_directory=$2
	file=$3
	NUM=$4
	
	echo " Displaying the content at the end of a file  "
	tail -${NUM} ${parent_directory}/${file}
fi

if [ ${operation} == "showFileContentAtLine"  ] 
then 
	parent_directory=$2
	file=$3
	NUM=$4
	
	echo " Showing file content line by line "
	head -${NUM} ${parent_directory}/${file} | tail +${NUM}
fi

if [ ${operation} == "showFileContentForLineRange"  ] 
then 
	parent_directory=$2
	file=$3
	START=$4
	END=$5
	echo " Showing file content over a range  "
	head -${END} ${parent_directory}/${file} | tail -${START}
fi

if [ ${operation} == "moveFile"  ] 
then 
	parent_directory=$2
	child_directory=$3
	echo " Moving a file "
	mv ${parent_directory} ${child_directory}
fi


if [ ${operation} == "copyFile"  ] 
then 
	parent_directory=$2
	child_directory=$3
	echo " Copying a file "
	cp ${parent_directory} ${child_directory}
fi

if [ ${operation} == "clearFileContent"  ] 
then 
	parent_directory=$2
	file=$3
	echo " Clearing file content "
	echo " " > ${parent_directory}/${file}
fi


if [ ${operation} == "deleteFile"  ] 
then 
	parent_directory=$2
	file=$3
	echo " Deletinga file "
	rm -rf ${parent_directory}/${file}
fi
